﻿using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Application.Contracts
{
    public interface IAppServiceEndereco
         : IAppServiceBase<Endereco>
    {
        List<Endereco> FindByIdPaciente(int id);
    }
}
