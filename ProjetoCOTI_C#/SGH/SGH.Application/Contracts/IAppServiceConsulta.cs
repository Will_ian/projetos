﻿using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Application.Contracts
{
    public interface IAppServiceConsulta
        :IAppServiceBase<Consulta>
    {
        List<Consulta> FindAllByData(DateTime dataConsulta);
        List<Consulta> FindAllBetweenData(DateTime dataInicial, DateTime dataFinal);
        Consulta FindAllByDataHour(DateTime dataConsulta, DateTime horaConsulta);
        List<Consulta> FindAllByDataAndMedico(DateTime dataConsulta, int idMedico);
    }
}
