﻿using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Application.Contracts
{
    public interface IAppServiceMedicacao
        : IAppServiceBase<Medicacao>
    {
        List<Medicacao> FindAllByName(string nome);
    }
}
