﻿using SGH.Application.Contracts;
using SGH.Domain.Contracts.Services;
using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Application.Services
{
    public class AppServiceMedico
       : AppServiceBase<Medico>,
       IAppServiceMedico
    {

        private IDomainServiceMedico dominio;

        public AppServiceMedico(IDomainServiceMedico dominio)
            : base(dominio)
        {
            this.dominio = dominio;
        }

       
        public List<Medico> FindAllByName(string nome)
        {
            return this.dominio.FindAllByName(nome);
        }

        public Medico FindByCrm(int CRM)
        {
            return this.dominio.FindByCrm(CRM);
        }
    }
}
