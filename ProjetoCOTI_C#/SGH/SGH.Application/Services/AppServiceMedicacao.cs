﻿using SGH.Application.Contracts;
using SGH.Domain.Contracts.Services;
using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Application.Services
{
    public class AppServiceMedicacao
        : AppServiceBase<Medicacao>,
        IAppServiceMedicacao
    {

        private IDomainServiceMedicacao dominio;

        public AppServiceMedicacao(IDomainServiceMedicacao dominio)
            :base (dominio)
        {
            this.dominio = dominio;
        }
        public List<Medicacao> FindAllByName(string nome)
        {
            return this.dominio.FindAllByName(nome);
        }
    }
}
