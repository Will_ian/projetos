﻿using SGH.Application.Contracts;
using SGH.Domain.Contracts.Services;
using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Application.Services
{
    public class AppServiceTelefone : AppServiceBase<Telefone>,
        IAppServiceTelefone
    {

        private IDomainServiceTelefone dominio;

        public AppServiceTelefone(IDomainServiceTelefone dominio)
            :base(dominio)
        {
            this.dominio = dominio;
        }

        public List<Telefone> FindByIdPaciente(int id)
        {
            return this.dominio.FindByIdPaciente(id);
        }
    }
}
