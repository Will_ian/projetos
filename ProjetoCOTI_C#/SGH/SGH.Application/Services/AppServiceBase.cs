﻿using SGH.Application.Contracts;
using SGH.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Application.Services
{
    public class AppServiceBase<TEntity>
        : IAppServiceBase<TEntity>
        where TEntity : class
    {
        private IDomainServiceBase<TEntity> dominio;

        public AppServiceBase(IDomainServiceBase<TEntity> dominio)
        {
            this.dominio = dominio;
        }  

        public void Create(TEntity obj)
        {
            this.dominio.Create(obj);
        }

        public void Update(TEntity obj)
        {
            this.dominio.Update(obj);
        }

        public void Delete(TEntity obj)
        {
            this.dominio.Delete(obj);
        }

        public List<TEntity> FindAll()
        {
            return this.dominio.FindAll();
        }

        public TEntity FindById(int id)
        {
            return this.dominio.FindById(id);
        }        
    }
}
