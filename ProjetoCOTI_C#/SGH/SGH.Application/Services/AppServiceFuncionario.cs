﻿using SGH.Application.Contracts;
using SGH.Domain.Contracts.Services;
using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Application.Services
{
    public class AppServiceFuncionario
        : AppServiceBase<Funcionario>,
        IAppServiceFuncionario
    {

        private IDomainServiceFuncionario dominio;

        public AppServiceFuncionario(IDomainServiceFuncionario dominio)
            :base(dominio)
        {
            this.dominio = dominio;
        }

        public Funcionario FindByEmail(string login)
        {
            return this.dominio.FindByEmail(login);
        }

        public Funcionario FindByEmailPassword(string login, string senha)
        {
           return this.dominio.FindByEmailPassword(login, senha);
        }

        public List<Funcionario> FindAllByName(string nome)
        {
            return this.dominio.FindAllByName(nome);
        }
    }
}
