﻿using SGH.Application.Contracts;
using SGH.Domain.Contracts.Services;
using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Application.Services
{
    public class AppServiceEndereco : AppServiceBase<Endereco>,
        IAppServiceEndereco
    {

        private IDomainServiceEndereco dominio;

        public AppServiceEndereco(IDomainServiceEndereco dominio)
            :base(dominio)
        {
            this.dominio = dominio;
        }

        public List<Endereco> FindByIdPaciente(int id)
        {
            return this.dominio.FindByIdPaciente(id);
        }
    }
}
