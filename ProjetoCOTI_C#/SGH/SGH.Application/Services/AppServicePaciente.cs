﻿using SGH.Application.Contracts;
using SGH.Domain.Contracts.Services;
using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Application.Services
{
    public class AppServicePaciente : AppServiceBase<Paciente>,
        IAppServicePaciente
    {

        private IDomainServicePaciente dominio;

        public AppServicePaciente(IDomainServicePaciente dominio)
            :base(dominio)
        {
            this.dominio = dominio;
        }

        public List<Paciente> FindAllByName(string nome)
        {
            return this.dominio.FindAllByNome(nome);
        }
    }
}
