﻿using SGH.Application.Contracts;
using SGH.Domain.Contracts.Services;
using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Application.Services
{
    public class AppServiceConsulta
        : AppServiceBase<Consulta>,
        IAppServiceConsulta
    {
        private IDomainServiceConsulta dominio;

        public AppServiceConsulta(IDomainServiceConsulta dominio)
            :base(dominio)
        {
            this.dominio = dominio;
        }
        public List<Consulta> FindAllByData(DateTime dataConsulta)
        {
            return this.dominio.FindAllByData(dataConsulta);
        }
        public Consulta FindAllByDataHour(DateTime dataConsulta, DateTime horaConsulta)
        {
            return this.dominio.FindAllByDataHour(dataConsulta, horaConsulta);
        }
        public List<Consulta> FindAllByDataAndMedico(DateTime dataConsulta, int idMedico)
        {
            return this.dominio.FindAllByDataAndMedico(dataConsulta, idMedico);
        }

        public List<Consulta> FindAllBetweenData(DateTime dataInicial, DateTime dataFinal)
        {
            return this.dominio.FindAllBetweenData(dataInicial, dataFinal);
        }
    }
}
