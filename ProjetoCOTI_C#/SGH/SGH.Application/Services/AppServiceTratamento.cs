﻿using SGH.Application.Contracts;
using SGH.Domain.Contracts.Services;
using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Application.Services
{
    public class AppServiceTratamento
        : AppServiceBase<Tratamento>,
        IAppServiceTratamento
    {
        private IDomainServiceTratamento dominio;

        public AppServiceTratamento(IDomainServiceTratamento dominio)
            : base(dominio)
        {
            this.dominio = dominio;
        }

        public Tratamento FindByIdConsulta(int id)
        {
            return this.dominio.FindByIdConsulta(id);
        }

        public Tratamento FindByIdMedicacao(int id)
        {
            return this.dominio.FindByIdMedicacao(id);
        }
    }
}
