﻿using Microsoft.Reporting.WebForms;
using SGH.Entities;
using SGH.Infra.Repository.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SGH.Web.Reports
{
    public partial class ReportPaciente : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnPesquisa_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime dataInicial = DateTime.Parse(txtDataIni.Text);
                DateTime dataFinal = DateTime.Parse(txtDataFim.Text);

                RepositoryPaciente t = new RepositoryPaciente();
                List<Paciente> c = t.FindAllBetweenData(dataInicial, dataFinal);

                //gerar relatório
                //1) Obter o caminho do relatório no projeto
                string path = HttpContext.Current.Server.MapPath("/Reports/RelatorioPacientes.rdlc");

                //2) gerar os parâmetros do relatório
                ReportParameter[] parametros = new ReportParameter[2];
                parametros[0] = new ReportParameter("DataInicial", dataInicial.ToString());
                parametros[1] = new ReportParameter("DataFinal", dataFinal.ToString());

                //3)definir o dataset do relatório (lista)
                ReportDataSource dataSet = new ReportDataSource("DataSetPaciente", c);

                //montar tudo no ReportViewer

                ReportViewerPaciente.Reset();
                ReportViewerPaciente.LocalReport.ReportPath = path;
                ReportViewerPaciente.LocalReport.SetParameters(parametros);
                ReportViewerPaciente.LocalReport.DataSources.Add(dataSet);
                            
                ReportViewerPaciente.DataBind();
            }
            catch (Exception ex)
            {
                lblMensagem.Text = "Erro: " + ex.Message;
            }
        }
    }
}