﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportPaciente.aspx.cs" Inherits="SGH.Web.Reports.ReportPaciente" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="~/Content/bootstrap-theme.min.css" rel="stylesheet" />
    <link href="~/Content/bootstrap.min.css" rel="stylesheet" />
    
    <script src="../Scripts/jquery-1.9.1.min.js"></script>
    <script src="../Scripts/jquery-1.9.1.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

   
    <script src="../Scripts/bootstrap.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <asp:ScriptManager ID="scriptManager" runat="server" />

        <asp:UpdatePanel ID="painelAjax" runat="server">
            <ContentTemplate>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <br />
                    <div class="col-xs-4">
                        <label>Data de Nascimento Inicial:</label>
                     <asp:TextBox ID="txtDataIni" type="date" runat="server" CssClass="form-control"/>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <br />
                    <div class="col-xs-4">
                       <label>Data de Nascimento Final:</label>
                     <asp:TextBox ID="txtDataFim" type="date" runat="server" CssClass="form-control"/>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <br />
                    <div class="col-xs-4">
                      <asp:Button ID="btnPesquisa" runat="server"
                                Text="Pesquisar Pacientes" CssClass="btn btn-primary btn-sm" 
                                onclick="btnPesquisa_Click"/> 
                    </div>
                </div>
                
                <hr />
                <asp:Label ID="lblMensagem" runat="server" ForeColor="Red" />
                <br /><br />
                <div class="col-md-12">

                    <rsweb:ReportViewer 
                        ID="ReportViewerPaciente" 
                        runat="server" 
                        Width="1000"
                        Height="600"
                        SizeToReportContent="true" style="margin-left: 200px" 
                        >
                    </rsweb:ReportViewer>

                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
