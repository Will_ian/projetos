﻿using Microsoft.Reporting.WebForms;
using SGH.Application.Contracts;
using SGH.Application.Services;
using SGH.Domain.Contracts.Services;
using SGH.Entities;
using SGH.Infra.Repository.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SGH.Web.Reports
{
    public partial class ReportConsulta : System.Web.UI.Page
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnPesquisa_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime dataInicial = DateTime.Parse(txtDataIni.Text);
                DateTime dataFinal = DateTime.Parse(txtDataFim.Text);

                RepositoryConsulta t = new RepositoryConsulta();
                List<Consulta> c = t.FindAllBetweenData(dataInicial, dataFinal);

                //gerar relatório
                //1) Obter o caminho do relatório no projeto
                string path = HttpContext.Current.Server.MapPath("/Reports/RelatorioConsultas.rdlc");

                //2) gerar os parâmetros do relatório
                ReportParameter[] parametros = new ReportParameter[2];
                parametros[0] = new ReportParameter("DataInicial", dataInicial.ToString());
                parametros[1] = new ReportParameter("DataFinal", dataFinal.ToString());

                //3)definir o dataset do relatório (lista)
                ReportDataSource dataSet = new ReportDataSource("DataSetConsulta", c);

                //montar tudo no ReportViewer

                ReportViewerConsulta.Reset();
                ReportViewerConsulta.LocalReport.ReportPath = path;
                ReportViewerConsulta.LocalReport.SetParameters(parametros);
                ReportViewerConsulta.LocalReport.DataSources.Add(dataSet);

                ReportViewerConsulta.DataBind();
            }
            catch (Exception ex)
            {
                lblMensagem.Text = "Erro: " + ex.Message;
            }
        }
    }
}