﻿using Microsoft.Reporting.WebForms;
using SGH.Entities;
using SGH.Infra.Repository.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SGH.Web.Reports
{
    public partial class ReportTratamento : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnPesquisa_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime dataInicial = DateTime.Parse(txtDataIni.Text);
                DateTime dataFinal = DateTime.Parse(txtDataFim.Text);
                int horaInicial = Int32.Parse(txtHoraIni.Text);
                int horaFinal = Int32.Parse(txtHoraFim.Text);

                RepositoryConsulta consulta = new RepositoryConsulta();
                List<Consulta> c = consulta.FindAllBetweenDataHour(dataInicial.AddHours(horaInicial), dataFinal.AddHours(horaFinal));
                List<Tratamento> tratamento = new List<Tratamento>();
                RepositoryTratamento t = new RepositoryTratamento();

                foreach (Consulta co in c)
                {
                    Tratamento ta = t.FindByIdConsulta(co.IdConsulta);
                    if (ta != null)
                    {
                        tratamento.Add(ta);
                    }
                }
                //gerar relatório
                //1) Obter o caminho do relatório no projeto
                string path = HttpContext.Current.Server.MapPath("/Reports/RelatorioTratamentos.rdlc");

                //2) gerar os parâmetros do relatório
                ReportParameter[] parametros = new ReportParameter[4];
                parametros[0] = new ReportParameter("DataInicial", dataInicial.ToString());
                parametros[1] = new ReportParameter("DataFinal", dataFinal.ToString());
                parametros[2] = new ReportParameter("HoraInicial", horaInicial.ToString());
                parametros[3] = new ReportParameter("HoraFinal", horaFinal.ToString());

                //3)definir o dataset do relatório (lista)
                ReportDataSource dataSet = new ReportDataSource("DataSetTratamento", tratamento);

                //montar tudo no ReportViewer

                ReportViewerTratamento.Reset();
                ReportViewerTratamento.LocalReport.ReportPath = path;
                ReportViewerTratamento.LocalReport.SetParameters(parametros);
                ReportViewerTratamento.LocalReport.DataSources.Add(dataSet);
                            
                ReportViewerTratamento.DataBind();
            }
            catch (Exception ex)
            {
                lblMensagem.Text = "Erro: " + ex.Message;
            }
        }
    }
}