﻿using SGH.Application.Contracts;
using SGH.Entities;
using SGH.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SGH.Web.Controllers
{
    [Authorize]
    public class FuncionarioController : Controller
    {
        private IAppServiceFuncionario appFuncionario;

        public FuncionarioController(IAppServiceFuncionario appFuncionario)
        {
            this.appFuncionario = appFuncionario;
        }

        [HttpGet, OutputCache(NoStore = true, Duration = 1)]
        // GET: Funcionario
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        public JsonResult Salvar(string nomeFuncionario, string loginFuncionario, string senhaFuncionario)
        {
            try
            {
                if (appFuncionario.FindByEmail(loginFuncionario) != null)
                {
                    return Json("Email já cadastrado");
                }
                else
                {
                    Funcionario fu = new Funcionario()
                    {
                        Nome = nomeFuncionario,
                        Login = loginFuncionario,
                        Senha = Criptografia.EncriptarSenha(senhaFuncionario),
                        DataCadastro = DateTime.Today
                    };

                    appFuncionario.Create(fu);
                    return Json(fu);
                } 
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }

        }

        public JsonResult Alterar(string nomeFuncionario, string loginFuncionario)
        {
            try
            {               
                Funcionario f = appFuncionario.FindByEmail(loginFuncionario);

                f.Nome = nomeFuncionario;
                f.Login = loginFuncionario;
                
                appFuncionario.Update(f);
                return Json(f);
                
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }

        }
        public JsonResult CarregarFuncionarios()
        {
            try
            {
                StringBuilder dados = new StringBuilder();

                foreach (Funcionario f in appFuncionario.FindAll())
                {                    
                    dados.Append("<tr>");
                    dados.AppendFormat("<td> {0} </td>", f.IdFuncionario);
                    dados.AppendFormat("<td> {0} </td>", f.Nome);
                    dados.AppendFormat("<td> {0} </td>", f.Login);
                    dados.AppendFormat("<td> {0:dd/MM/yyyy} </td>", f.DataCadastroFormatada);
                    dados.AppendFormat("<td> <a href='Update?id={0}' class='btn btn-warning btn-sm'> Alterar </a>  ", f.IdFuncionario);
                    dados.AppendFormat("<a href='Delete?id={0}' class='btn btn-danger btn-sm'> Excluir </a></td>", f.IdFuncionario);
                    dados.Append("</tr>");
                }

                return Json(dados.ToString());
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        public JsonResult ConsultarFuncionarios(string nome)
        {
            try
            {
                StringBuilder dados = new StringBuilder();

                foreach (Funcionario f in appFuncionario.FindAllByName(nome))
                {
                    dados.Append("<tr>");
                    dados.AppendFormat("<td> {0} </td>", f.IdFuncionario);
                    dados.AppendFormat("<td> {0} </td>", f.Nome);
                    dados.AppendFormat("<td> {0} </td>", f.Login);
                    dados.AppendFormat("<td> {0:dd/MM/yyyy} </td>", f.DataCadastroFormatada);
                    dados.AppendFormat("<td> <a href='Update?id={0}' class='btn btn-warning btn-sm'> Alterar </a>  ", f.IdFuncionario);
                    dados.AppendFormat("<a href='Delete?id={0}' class='btn btn-danger btn-sm'> Excluir </a></td>", f.IdFuncionario);
                    dados.Append("</tr>");
                }

                return Json(dados.ToString());
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        public ActionResult Update(int id)
        {
            Funcionario f = appFuncionario.FindById(id);
            return View(f);
        }
        public ActionResult Delete(int id)
        {
            try
            {
                Funcionario f = appFuncionario.FindById(id);
                if (f != null)
                {
                    appFuncionario.Delete(f);
                    return RedirectToAction("Index", "Funcionario");
                }

                return RedirectToAction("Index", "Paciente");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Paciente");
                // return Json(ex.Message);
            }
        }
    }
}