﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SGH.Web.Controllers
{
    [Authorize] //somente usuários autenticados
    public class TratamentoHomeController : Controller
    {
        // GET: Tratamento
        public ActionResult Index()
        {
            return View();
        }
    }
}