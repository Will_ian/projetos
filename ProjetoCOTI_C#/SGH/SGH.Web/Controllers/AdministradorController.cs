﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SGH.Web.Controllers
{
    [Authorize] //somente usuários autenticados
    public class AdministradorController : Controller
    {
        [HttpGet, OutputCache(NoStore = true, Duration = 1)]
        // GET: Administrador
        public ActionResult Index()
        {
            return View();
        }     
    }
}