﻿using SGH.Application.Contracts;
using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SGH.Web.Controllers
{
    [Authorize] //somente usuários autenticados
    public class MedicacaoController : Controller
    {
        private IAppServiceMedicacao appMedicacao;

        public MedicacaoController(IAppServiceMedicacao appMedicacao)
        {
            this.appMedicacao = appMedicacao;
        }
        // GET: Medicacao
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Create()
        {
            return View();
        }

        public JsonResult Salvar(string nomeMedicacao, string descricaoMedicacao)
        {
            try
            {
                Medicacao m = new Medicacao()
                {
                    Nome = nomeMedicacao,
                    Descricao = descricaoMedicacao
                };

                appMedicacao.Create(m);
                                   
                return Json(m);
               
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }

        }

        public JsonResult Alterar(int idMedicacao, string nomeMedicacao, string descricaoMedicacao)
        {
            try
            {
                Medicacao m = appMedicacao.FindById(idMedicacao);

                m.Nome = nomeMedicacao;
                m.Descricao = descricaoMedicacao;

                appMedicacao.Update(m);
                return Json(m);

            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }

        }
        public JsonResult CarregarMedicamentos()
        {
            try
            {
                StringBuilder dados = new StringBuilder();

                foreach (Medicacao m in appMedicacao.FindAll())
                {
                    dados.Append("<tr>");
                    dados.AppendFormat("<td> {0} </td>", m.IdMedicacao);
                    dados.AppendFormat("<td> {0} </td>", m.Nome);
                    dados.AppendFormat("<td> {0} </td>", m.Descricao);                   
                    dados.AppendFormat("<td> <a href='Update?id={0}' class='btn btn-warning btn-sm'> Alterar </a>  ", m.IdMedicacao);
                    dados.AppendFormat("<a href='Delete?id={0}' class='btn btn-danger btn-sm'> Excluir </a></td>", m.IdMedicacao);
                    dados.Append("</tr>");
                }

                return Json(dados.ToString());
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        public JsonResult ConsultarMedicamentos(string nome)
        {
            try
            {
                StringBuilder dados = new StringBuilder();

                foreach (Medicacao m in appMedicacao.FindAllByName(nome))
                {
                    dados.Append("<tr>");
                    dados.AppendFormat("<td> {0} </td>", m.IdMedicacao);
                    dados.AppendFormat("<td> {0} </td>", m.Nome);
                    dados.AppendFormat("<td> {0} </td>", m.Descricao);
                    dados.AppendFormat("<td> <a href='Update?id={0}' class='btn btn-warning btn-sm'> Alterar </a>  ", m.IdMedicacao);
                    dados.AppendFormat("<a href='Delete?id={0}' class='btn btn-danger btn-sm'> Excluir </a></td>", m.IdMedicacao);
                    dados.Append("</tr>");
                }

                return Json(dados.ToString());
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        public ActionResult Update(int id)
        {
            Medicacao m = appMedicacao.FindById(id);
            return View(m);
        }
        public ActionResult Delete(int id)
        {
            try
            {
                Medicacao m = appMedicacao.FindById(id);
                if (m != null)
                {
                    appMedicacao.Delete(m);
                    return RedirectToAction("Index", "Funcionario");
                }

                return RedirectToAction("Index", "Paciente");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Paciente");
                // return Json(ex.Message);
            }
        }

        public ActionResult ListarMedicamentos()
        {
            List<Medicacao> m = appMedicacao.FindAll();
            return Json(m, JsonRequestBehavior.AllowGet);
        }
    }
}