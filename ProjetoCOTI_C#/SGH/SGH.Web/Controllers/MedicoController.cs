﻿using SGH.Application.Contracts;
using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SGH.Web.Controllers
{
    [Authorize]
    public class MedicoController : Controller
    {
        private IAppServiceMedico appMedico;

        public MedicoController(IAppServiceMedico appMedico)
        {
            this.appMedico = appMedico;
        }

        // GET: Medico
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        public JsonResult Salvar(string nomeMedico, string crmMedico, string descricaoMedico)
        {
            try
            {
                if (appMedico.FindByCrm(Int32.Parse(crmMedico)) != null)
                {
                    return Json("CRM já cadastrado");
                }
                else
                {
                    Medico m = new Medico()
                    {
                       Nome = nomeMedico,
                       CRM = Int32.Parse(crmMedico),
                       Descricao = descricaoMedico
                    };

                    appMedico.Create(m);
                    return Json(m);
                }
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }

        }

        public JsonResult Alterar(string nomeMedico, string crmMedico, string descricaoMedico)
        {
            try
            {
                Medico m = appMedico.FindByCrm(Int32.Parse(crmMedico));

                m.Nome = nomeMedico;
                m.Descricao = descricaoMedico;

                appMedico.Update(m);
                return Json(m);

            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }

        }
        public JsonResult CarregarMedicos()
        {
            try
            {
                StringBuilder dados = new StringBuilder();

                foreach (Medico m in appMedico.FindAll())
                {
                    dados.Append("<tr style='text - align:center'>");
                    dados.AppendFormat("<td> {0} </td>", m.IdMedico);
                    dados.AppendFormat("<td> {0} </td>", m.Nome);
                    dados.AppendFormat("<td> {0} </td>", m.CRM);
                    dados.AppendFormat("<td> {0} </td>", m.Descricao);
                    dados.AppendFormat("<td> <a href='Update?id={0}' class='btn btn-warning btn-sm'> Alterar </a>  ", m.IdMedico);
                    dados.AppendFormat("<a href='Delete?id={0}' class='btn btn-danger btn-sm'> Excluir </a></td>", m.IdMedico);
                    dados.Append("</tr>");
                }

                return Json(dados.ToString());
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        public JsonResult ConsultarMedicos(string nome)
        {
            try
            {
                StringBuilder dados = new StringBuilder();

                foreach (Medico m in appMedico.FindAllByName(nome))
                {
                    dados.Append("<tr>");
                    dados.AppendFormat("<td> {0} </td>", m.IdMedico);
                    dados.AppendFormat("<td> {0} </td>", m.Nome);
                    dados.AppendFormat("<td> {0} </td>", m.CRM);
                    dados.AppendFormat("<td> {0} </td>", m.Descricao);
                    dados.AppendFormat("<td> <a href='Update?id={0}' class='btn btn-warning btn-sm'> Alterar </a>  ", m.IdMedico);
                    dados.AppendFormat("<a href='Delete?id={0}' class='btn btn-danger btn-sm'> Excluir </a></td>", m.IdMedico);
                    dados.Append("</tr>");
                }

                return Json(dados.ToString());
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        public ActionResult Update(int id)
        {
            Medico m = appMedico.FindById(id);
            return View(m);
        }

        public ActionResult Delete(int id)
        {
            try
            {
                Medico m = appMedico.FindById(id);
                if (m != null)
                {
                    appMedico.Delete(m);
                    return RedirectToAction("Index", "Medico");
                }

                return RedirectToAction("Index", "Medico");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Medico");
                // return Json(ex.Message);
            }
        }

        public ActionResult ListarMedicos()
        {
            List<Medico> m = appMedico.FindAll();
            return Json(m, JsonRequestBehavior.AllowGet);
        }
    }
}