﻿using SGH.Application.Contracts;
using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SGH.Web.Controllers
{
    public class TratamentoController : Controller
    {
        private IAppServiceTratamento appTratamento;
        private IAppServiceConsulta appConsulta;
        private IAppServiceMedicacao appMedicacao;

        public TratamentoController(IAppServiceTratamento appTratamento, IAppServiceConsulta appConsulta, IAppServiceMedicacao appMedicacao)
        {
            this.appTratamento = appTratamento;
            this.appConsulta = appConsulta;
            this.appMedicacao = appMedicacao;
        }
        // GET: Tratamento
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Update(int id)
        {
            Tratamento t = appTratamento.FindById(id);
            t.Consulta = appConsulta.FindById(t.IdConsulta);
            t.Medicacao = appMedicacao.FindById(t.IdMedicacao);

            return View(t);
        }
        public JsonResult Salvar(string descricaoTratamento, string idConsulta, string idMedicacao)
        {
            try
            {
                Tratamento t = new Tratamento()
                {
                    Descricao = descricaoTratamento,
                    IdConsulta = Int32.Parse(idConsulta),
                    IdMedicacao = Int32.Parse(idMedicacao)
                };

                appTratamento.Create(t);
                return Json(t);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        public JsonResult Alterar(string idTratamento, string descricaoTratamento, string idConsulta, string idMedicacao)
        {
            try
            {
                Tratamento t = appTratamento.FindById(Int32.Parse(idTratamento));

                t.Descricao = descricaoTratamento;
                t.IdConsulta = Int32.Parse(idConsulta);
                t.IdMedicacao = Int32.Parse(idMedicacao);

                appTratamento.Update(t);
                return Json(t);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        public ActionResult Delete(int id)
        {
            Tratamento t = appTratamento.FindById(id);
            if (t != null)
            {
                appTratamento.Delete(t);
            }
            return RedirectToAction("Index", "Tratamento");
        }

        public JsonResult CarregarTratamentos()
        {
            try
            {
                StringBuilder dados = new StringBuilder();

                foreach (Tratamento t in appTratamento.FindAll())
                {
                    t.Consulta = appConsulta.FindById(t.IdConsulta);
                    t.Medicacao = appMedicacao.FindById(t.IdMedicacao);

                    dados.Append("<tr style='text - align:center'>");
                    dados.AppendFormat("<td> {0} </td>", t.IdTratamento);
                    dados.AppendFormat("<td> {0} </td>", t.Descricao);
                    dados.AppendFormat("<td> {0} - {1} </td>", t.Consulta.IdConsulta, t.Consulta.Hora);
                    dados.AppendFormat("<td> {0} - {1} </td>", t.Medicacao.IdMedicacao, t.Medicacao.Nome);
                    if (t.Consulta.Data >= DateTime.Today)
                    {
                        dados.AppendFormat("<td> <a href='Update?id={0}' class='btn btn-warning btn-sm'> Alterar </a>  ", t.IdTratamento);
                        dados.AppendFormat("<a href='Delete?id={0}' class='btn btn-danger btn-sm'> Excluir </a></td>", t.IdTratamento);
                    }
                    else
                    {
                        dados.Append("<td></td>");
                    }
                    dados.Append("</tr>");
                }

                return Json(dados.ToString());
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        public JsonResult ConsultarTratamentos(string dataConsulta)
        {
            try
            {
                StringBuilder dados = new StringBuilder();

                foreach (Consulta c in appConsulta.FindAllByData(DateTime.Parse(dataConsulta)))
                {
                    if (c != null)
                    {
                        Tratamento t = appTratamento.FindByIdConsulta(c.IdConsulta);

                        if (t != null)
                        {
                            t.Consulta = appConsulta.FindById(t.IdConsulta);
                            t.Medicacao = appMedicacao.FindById(t.IdMedicacao);

                            dados.Append("<tr style='text - align:center'>");
                            dados.AppendFormat("<td> {0} </td>", t.IdTratamento);
                            dados.AppendFormat("<td> {0} </td>", t.Descricao);
                            dados.AppendFormat("<td> {0} - {1} </td>", t.Consulta.IdConsulta, t.Consulta.Hora);
                            dados.AppendFormat("<td> {0} - {1} </td>", t.Medicacao.IdMedicacao, t.Medicacao.Nome);
                            if (t.Consulta.Data >= DateTime.Today)
                            {
                                dados.AppendFormat("<td> <a href='Update?id={0}' class='btn btn-warning btn-sm'> Alterar </a>  ", t.IdTratamento);
                                dados.AppendFormat("<a href='Delete?id={0}' class='btn btn-danger btn-sm'> Excluir </a></td>", t.IdTratamento);
                            }
                            else
                            {
                                dados.Append("<td></td>");
                            }
                            dados.Append("</tr>");
                        }
                        
                    }
                    
                }

                return Json(dados.ToString());
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
    }
}