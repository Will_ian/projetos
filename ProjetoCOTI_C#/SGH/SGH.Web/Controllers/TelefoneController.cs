﻿using SGH.Application.Contracts;
using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SGH.Web.Controllers
{
    [Authorize] //somente usuários autenticados
    public class TelefoneController : Controller
    {
        private IAppServiceTelefone appTelefone;

        public TelefoneController(IAppServiceTelefone appTelefone)
        {          
            this.appTelefone = appTelefone;
        }

        private List<Telefone> telefone;
        private List<Telefone> telefoneExclusao;
        // GET: Telefone
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult AdicionarTelefoneSession(string telefoneNumero, string tipoTelefone)
        {
            //Session.Abandon();
            Telefone t = new Telefone
            {
                NumeroTel = long.Parse(telefoneNumero),
                TipoTel = tipoTelefone
            };  
               

            if (Session["telefoneSession"] != null)
            {
                telefone = Session["telefoneSession"] as List<Telefone>;
                foreach (Telefone tel in telefone)
                {
                    if (tel.NumeroTel == long.Parse(telefoneNumero))
                    {
                        return Json("Telefone já cadastrado");
                    }
                }
                telefone.Add(t);
            }
            else
            {
                telefone = new List<Telefone>();
                telefone.Add(t);                 
            }

            Session["telefoneSession"] = telefone;
            return Json(t);
        }

        public JsonResult AdicionarTelefoneSessionExclusao(string id)
        {
            Telefone t = appTelefone.FindById(Int32.Parse(id));


            if (Session["telefoneSessionExclusao"] != null)
            {
                telefoneExclusao = Session["telefoneSessionExclusao"] as List<Telefone>;
                telefoneExclusao.Add(t);
            }
            else
            {
                telefoneExclusao = new List<Telefone>();
                telefoneExclusao.Add(t);
            }

            Session["telefoneSessionExclusao"] = telefoneExclusao;
            telefone = Session["telefoneSession"] as List<Telefone>;
            if (telefone.FindIndex(te => te.IdTelefone == t.IdTelefone) >= 0)
            {
                telefone.RemoveAll(te => te.IdTelefone == t.IdTelefone);
                Session["telefoneSession"] = telefone;
            }
            return this.Json("Telefone excluido com sucesso");
        }

        public JsonResult ExcluirTelefoneSession(string tel)
        {
            if (Session["telefoneSession"] != null)
            {
                telefone = Session["telefoneSession"] as List<Telefone>;
                Telefone t;
                t = telefone.Where(te => te.NumeroTel == long.Parse(tel)).FirstOrDefault();

                if (t != null)
                {
                    telefone.Remove(t);
                    Session["telefoneSession"] = telefone;
                    return this.Json("Telefone excluido com sucesso");
                }
                return this.Json("");
            }
            return this.Json("");
        }
    }
}