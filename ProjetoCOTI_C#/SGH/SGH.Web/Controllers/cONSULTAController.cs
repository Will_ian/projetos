﻿using SGH.Application.Contracts;
using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SGH.Web.Controllers
{
    [Authorize] //somente usuários autenticados
    public class ConsultaController : Controller
    {
        private IAppServiceConsulta appConsulta;
        private IAppServiceFuncionario appFuncionario;
        private IAppServiceMedico appMedico;
        private IAppServicePaciente appPaciente;
        public ConsultaController(IAppServiceConsulta appConsulta, IAppServiceFuncionario appFuncionario, 
                                        IAppServiceMedico appMedico, IAppServicePaciente appPaciente)
        {
            this.appConsulta = appConsulta;
            this.appFuncionario = appFuncionario;
            this.appMedico = appMedico;
            this.appPaciente = appPaciente;
        }
        // GET: Consulta
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            Funcionario f = Session["usuario_autenticado"] as Funcionario;
            return View(f);
        }

        public JsonResult Salvar(string dataConsulta, string horaConsulta, string valorConsulta, string funcionarioConsulta,
                                       string medicoConsulta, string pacienteConsulta, string receitaConsulta)
        {      
            try
            {
                Consulta c = new Consulta()
                {
                    Data = DateTime.Parse(dataConsulta),
                    Hora = DateTime.Parse(dataConsulta).AddHours(Int32.Parse(horaConsulta)),
                    ValorPago = Decimal.Parse(valorConsulta),
                    IdFuncionario = Int32.Parse(funcionarioConsulta),
                    IdMedico = Int32.Parse(medicoConsulta),
                    IdPaciente = Int32.Parse(pacienteConsulta),
                    Receita = receitaConsulta
                };

                appConsulta.Create(c);
                return Json(c);
                
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }

        }

        public JsonResult Alterar(string idConsulta, string dataConsulta, string horaConsulta, string valorConsulta,
                                       string medicoConsulta, string receitaConsulta)
        {
            try
            {
                Consulta c = appConsulta.FindById(Int32.Parse(idConsulta));

                c.Data = DateTime.Parse(dataConsulta);
                c.Hora = DateTime.Parse(dataConsulta).AddHours(Int32.Parse(horaConsulta));
                c.ValorPago = Decimal.Parse(valorConsulta);
                c.IdMedico = Int32.Parse(medicoConsulta);
                c.Receita = receitaConsulta;

                appConsulta.Update(c);
                return Json(c);

            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }

        }
        public ActionResult Update(int id)
        {
            Consulta c = appConsulta.FindById(id);
            c.Funcionario = appFuncionario.FindById(c.IdFuncionario);
            c.Medico = appMedico.FindById(c.IdMedico);
            c.Paciente = appPaciente.FindById(c.IdPaciente);

            return View(c);
        }

        public ActionResult Delete(int id)
        {
            try
            {
                Consulta c = appConsulta.FindById(id);
                if (c != null)
                {
                    appConsulta.Delete(c);
                    return RedirectToAction("Index", "Consulta");
                }
                return RedirectToAction("Index", "Funcionario");
            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Error");
            }
        }
        public JsonResult CarregarConsultas()
        {
            try
            {
                StringBuilder dados = new StringBuilder();

                foreach (Consulta c in appConsulta.FindAll().Where(co => co.Data >= DateTime.Today))
                {
                    c.Funcionario = appFuncionario.FindById(c.IdFuncionario);
                    c.Medico = appMedico.FindById(c.IdMedico);
                    c.Paciente = appPaciente.FindById(c.IdPaciente);

                    dados.Append("<tr>");
                    dados.AppendFormat("<td> {0} </td>", c.IdConsulta);
                    dados.AppendFormat("<td> {0:dd/MM/yyyy} </td>", c.Data);
                    dados.AppendFormat("<td> {0:00}:00 </td>", c.Hora.Hour);
                    dados.AppendFormat("<td> {0,00} </td>", c.ValorPago);
                    dados.AppendFormat("<td> {0} - {1} </td>",c.Funcionario.IdFuncionario, c.Funcionario.Login);
                    dados.AppendFormat("<td> {0} - {1} </td>",c.Medico.IdMedico, c.Medico.Nome);
                    dados.AppendFormat("<td> {0} - {1} </td>",c.Paciente.IdPaciente, c.Paciente.Nome);
                    dados.AppendFormat("<td> {0} </td>", c.Receita);
                    if (c.Data >= DateTime.Today)
                    {
                        dados.AppendFormat("<td> <a href='Update?id={0}' class='btn btn-warning btn-sm'> Alterar </a>  ", c.IdConsulta);
                        dados.AppendFormat("<a href='Delete?id={0}' class='btn btn-danger btn-sm'> Excluir </a></td>", c.IdConsulta);
                    }
                    else
                    {
                        dados.Append("<td></td>");
                    }
                    dados.Append("</tr>");
                }

                return Json(dados.ToString());
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        public JsonResult ConsultarConsultas(string dataConsulta)
        {
            try
            {
                StringBuilder dados = new StringBuilder();

                foreach (Consulta c in appConsulta.FindAllByData(DateTime.Parse(dataConsulta)))
                {
                    c.Funcionario = appFuncionario.FindById(c.IdFuncionario);
                    c.Medico = appMedico.FindById(c.IdMedico);
                    c.Paciente = appPaciente.FindById(c.IdPaciente);

                    dados.Append("<tr>");
                    dados.AppendFormat("<td> {0} </td>", c.IdConsulta);
                    dados.AppendFormat("<td> {0:dd/MM/yyyy} </td>", c.Data);
                    dados.AppendFormat("<td> {0:00}:00 </td>", c.Hora.Hour);
                    dados.AppendFormat("<td> {0,00} </td>", c.ValorPago);
                    dados.AppendFormat("<td> {0} - {1} </td>", c.Funcionario.IdFuncionario, c.Funcionario.Login);
                    dados.AppendFormat("<td> {0} - {1} </td>", c.Medico.IdMedico, c.Medico.Nome);
                    dados.AppendFormat("<td> {0} - {1} </td>", c.Paciente.IdPaciente, c.Paciente.Nome);
                    dados.AppendFormat("<td> {0} </td>", c.Receita);
                    if (c.Data >= DateTime.Today)
                    {
                        dados.AppendFormat("<td> <a href='Update?id={0}' class='btn btn-warning btn-sm'> Alterar </a>  ", c.IdConsulta);
                        dados.AppendFormat("<a href='Delete?id={0}' class='btn btn-danger btn-sm'> Excluir </a></td>", c.IdConsulta);
                    }
                    else
                    {
                        dados.Append("<td></td>");
                    }
                    dados.Append("</tr>");
                }

                return Json(dados.ToString());
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        public JsonResult ConsultarConsultasTratamento(string dataConsulta)
        {
            try
            {
                StringBuilder dados = new StringBuilder();

                foreach (Consulta c in appConsulta.FindAllByData(DateTime.Parse(dataConsulta)))
                {
                    
                    dados.Append("<tr <a href='#' title='Selecionar' onclick ='return preencherConsulta(\"" + c.Data + "\",\"" + c.Hora + "\")'></a>>");
                   
                    dados.AppendFormat("<td> {0:dd/MM/yyyy} </td>", c.Data);
                    dados.AppendFormat("<td> {0:00}:00 </td>", c.Hora.Hour);
               
                    dados.Append("</tr>");
                }

                return Json(dados.ToString());
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        public JsonResult ObterConsultaPorDataHora(string dataConsulta, string horaConsulta)
        {
            try
            {
                Consulta c = appConsulta.FindAllByDataHour(DateTime.Parse(dataConsulta), DateTime.Parse(horaConsulta));

                string consulta = c.IdConsulta.ToString() + " - " + c.Hora;

                return Json(consulta);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
        
        public JsonResult ListarConsultasPorData(string dataConsulta, int idMedico)
        {
            List<Consulta> consulta = appConsulta.FindAllByDataAndMedico(DateTime.Parse(dataConsulta), idMedico);
            StringBuilder dados = new StringBuilder();

            List<int> horasDisponiveis = new List<int>();
            for (int i = 7; i < 21; i++)
            {
                Consulta c = consulta.Where(cc => cc.HoraFormatada == i).FirstOrDefault();
                if(c == null)
                {
                    horasDisponiveis.Add(i);
                }               
            }


            if (consulta.Count > 0)
            {
                foreach (int i in horasDisponiveis)
                {
                    dados.AppendFormat("<option value='{0}'> {1}:00 </option>", i, i); 
                }       
             }
            else
            {
                for (var i = 7; i < 21; i++)
                {
                    dados.AppendFormat("<option value='{0}'> {1}:00 </option>", i, i);
                }
            }
            return Json(dados.ToString());
        }
        public JsonResult ListarHorarioDisponivel(string dataConsulta)
        {
            List<Consulta> c = appConsulta.FindAllByData(DateTime.Parse(dataConsulta));
            return Json(c);
        }
    }
}