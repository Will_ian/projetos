﻿using SGH.Application.Contracts;
using SGH.Entities;
using SGH.Helper.EnvioEmail;
using SGH.Util;
using SGH.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace SGH.Web.Controllers
{

    public class LoginController : Controller
    {
        private IAppServiceFuncionario appLogin;

        public LoginController(IAppServiceFuncionario appLogin)
        {
            this.appLogin = appLogin;
        }

        //public FuncionarioController()
        //{

        //}
        [HttpGet, OutputCache(NoStore = true, Duration = 1)]
        // GET: Usuario/Login
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Funcionario f = appLogin.FindByEmailPassword(model.LoginAcesso, Criptografia.EncriptarSenha(model.SenhaAcesso));

                    if (f != null)
                    {
                        //gerar ticket de acesso para usuário
                        FormsAuthenticationTicket ticket =
                            new FormsAuthenticationTicket(f.Login, false, 5); // false perde o ticket ao fechar o navegador/ true mantem o ticket

                        //gravar o ticket em arquivo -> cookie
                        HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName,
                                                           FormsAuthentication.Encrypt(ticket));

                        //gravar o cookie
                        Response.Cookies.Add(cookie);

                        Session.Add("usuario_autenticado", f); //jogando na sessão

                        //redireionar...
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        ViewBag.Mensagem = "Acesso Negado. Tente Novamente.";
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.Mensagem = ex.Message;
                }                
            }

            return View();
        }


        public ActionResult Logout()
        {
            //destruir o ticket de acesso..
            FormsAuthentication.SignOut();

            //redirecionando..
            return RedirectToAction("Login", "Login");
        }

        public ActionResult RecuperarSenha(LoginModel model)
        {
            Funcionario f = appLogin.FindByEmail(model.LoginAcesso);
            if (f != null)
            {
                Random generator = new Random();
                String password = generator.Next(100000, 999999).ToString("D6");

                if (MailHelper.SendMail(password, model.LoginAcesso))
                {
                    f.Senha = Criptografia.EncriptarSenha(password);
                    this.appLogin.Update(f);
                }              
            }
            return View("Login");
        }

    }
}