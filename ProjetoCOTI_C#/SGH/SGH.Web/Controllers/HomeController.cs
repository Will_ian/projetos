﻿using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SGH.Web.Controllers
{
    [Authorize] //somente usuários autenticados
    public class HomeController : Controller
    {
        // GET: Home
        [HttpGet, OutputCache(NoStore = true, Duration = 1)]
        public ActionResult Index()
        {
            Funcionario f = Session["usuario_autenticado"] as Funcionario;

            return View(f);
        }
    }
}