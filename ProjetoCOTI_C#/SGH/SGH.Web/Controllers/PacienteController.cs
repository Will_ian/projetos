﻿using SGH.Application.Contracts;
using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SGH.Web.Controllers
{
    [Authorize] //somente usuários autenticados
    public class PacienteController : Controller
    {
        private IAppServicePaciente appPaciente;
        private IAppServiceEndereco appEndereco;
        private IAppServiceTelefone appTelefone;

        public PacienteController(IAppServicePaciente appPaciente, IAppServiceEndereco appEndereco, IAppServiceTelefone appTelefone)
        {
            this.appPaciente = appPaciente;
            this.appEndereco = appEndereco;
            this.appTelefone = appTelefone;
        }

        [HttpGet, OutputCache(NoStore = true, Duration = 1)]
        // GET: Paciente
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Salvar(string nomePaciente, string emailPaciente, string sexoPaciente, string dataNascimento, 
                                    string logradouro, string bairro, string cidade, string estado, string cep)
        {
            try
            {
                Funcionario f = Session["usuario_autenticado"] as Funcionario;
                Paciente p = new Paciente()
                {
                    Nome = nomePaciente,
                    Email = emailPaciente,
                    Sexo = sexoPaciente,
                    DataNascimento = DateTime.Parse(dataNascimento),
                    IdFuncionario = f.IdFuncionario
                };
                
                Endereco e = new Endereco()
                {
                    Logradouro = logradouro,
                    Bairro = bairro,
                    Cidade = cidade,
                    Estado = estado,
                    Cep = Int32.Parse(cep)                   
                };
                
                p.Endereco = new List<Endereco>();
                p.Endereco.Add(e);
                p.Telefone = Session["telefoneSession"] as List<Telefone>;             
                appPaciente.Create(p);
                Session.Remove("telefoneSession");

                return RedirectToAction("Index", "Paciente");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Erro", "Paciente");
            }
            
        }

        public JsonResult Alterar(string idPaciente, string idEndereco, string nomePaciente, string emailPaciente, string sexoPaciente, string dataNascimento,
                                    string logradouro, string bairro, string cidade, string estado, string cep)
        {
            try
            {
                Funcionario f = Session["usuario_autenticado"] as Funcionario;
                Paciente p = appPaciente.FindById(Int32.Parse(idPaciente));

                p.Nome = nomePaciente;
                p.Email = emailPaciente;
                p.Sexo = sexoPaciente;
                p.DataNascimento = DateTime.Parse(dataNascimento);

                Endereco e = appEndereco.FindById(Int32.Parse(idEndereco));

                e.Logradouro = logradouro;
                e.Bairro = bairro;
                e.Cidade = cidade;
                e.Estado = estado;
                e.Cep = Int32.Parse(cep);
                

                p.Endereco = new List<Endereco>();
                p.Endereco.Add(e);

                List<Telefone> telExclusao = Session["telefoneSessionExclusao"] as List<Telefone>;
                if (telExclusao != null)
                {
                    foreach (Telefone t in telExclusao)
                    {                      
                        appTelefone.Delete(t);
                    }
                }

                List<Telefone> tel = Session["telefoneSession"] as List<Telefone>;
               
                if (tel != null)
                {
                    foreach (Telefone t in tel.Where(t => t.IdPaciente == 0))
                    {
                        t.IdPaciente = Int32.Parse(idPaciente);
                        appTelefone.Create(t);
                    }
                }

                appPaciente.Update(p);

                Session.Remove("telefoneSession");
                Session.Remove("telefoneSessionExclusao");
                return Json("");
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }

        }
        //[HttpGet]
        public JsonResult CarregarPacientes()
        {
            try
            {
                StringBuilder dados = new StringBuilder();

                foreach (Paciente p in appPaciente.FindAll())
                {
                    string telefone = "";
                    dados.Append("<tr>");
                    dados.AppendFormat("<td> {0} </td>", p.IdPaciente);
                    dados.AppendFormat("<td> {0} </td>", p.Nome);
                    dados.AppendFormat("<td> {0} </td>", p.Email);
                    dados.AppendFormat("<td> {0} </td>", p.Sexo);
                    dados.AppendFormat("<td> {0:dd/MM/yyyy} </td>", p.DataNascimento);
                    foreach (Telefone t in appTelefone.FindByIdPaciente(p.IdPaciente))
                    {
                        telefone = telefone + t.NumeroTel.ToString();
                    }
                    dados.AppendFormat("<td> {0} </td>", telefone);
                    foreach (Endereco e in appEndereco.FindByIdPaciente(p.IdPaciente))
                    {
                        dados.AppendFormat("<td> {0} </td>", e.Estado);
                        dados.AppendFormat("<td> {0} </td>", e.Cidade);
                        dados.AppendFormat("<td> {0} </td>", e.Bairro);
                        dados.AppendFormat("<td> {0} </td>", e.Cep);
                    }
                    dados.AppendFormat("<td> <a href='Update?id={0}' class='btn btn-warning btn-sm'> Alterar </a>  ", p.IdPaciente);
                    dados.AppendFormat("<a href='Delete?id={0}' class='btn btn-danger btn-sm'> Excluir </a></td>", p.IdPaciente);                  
                    dados.Append("</tr>");
                }

                return Json(dados.ToString());
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        public JsonResult ConsultarPacientes(string nome)
        {
            try
            {
                StringBuilder dados = new StringBuilder();
                string telefone = "";
                foreach (Paciente p in appPaciente.FindAllByName(nome))
                {
                    //p.Endereco = new List<Endereco>();
                    //p.Endereco = appEndereco.FindByIdPaciente(p.IdPaciente);
                    //p.Telefone = new List<Telefone>();
                    //p.Telefone = appTelefone.FindByIdPaciente(p.IdPaciente);
                                                                                                                                                
                    dados.Append("<tr>");
                    dados.AppendFormat("<td> {0} </td>", p.IdPaciente);
                    dados.AppendFormat("<td> {0} </td>", p.Nome);
                    dados.AppendFormat("<td> {0} </td>", p.Email);
                    dados.AppendFormat("<td> {0} </td>", p.Sexo);
                    dados.AppendFormat("<td> {0:dd/MM/yyyy} </td>", p.DataNascimento);
                    foreach (Telefone t in appTelefone.FindByIdPaciente(p.IdPaciente))
                    {
                        telefone = telefone + t.NumeroTel.ToString();
                    }
                    dados.AppendFormat("<td> {0} </td>", telefone);
                    foreach (Endereco e in appEndereco.FindByIdPaciente(p.IdPaciente))
                    {
                        dados.AppendFormat("<td> {0} </td>", e.Estado);
                        dados.AppendFormat("<td> {0} </td>", e.Cidade);
                        dados.AppendFormat("<td> {0} </td>", e.Bairro);
                        dados.AppendFormat("<td> {0} </td>", e.Cep);
                    }
                    dados.AppendFormat("<td> <a href='Update?id={0}' class='btn btn-warning btn-sm'> Alterar </a>  ", p.IdPaciente);
                    dados.AppendFormat("<a href='Delete?id={0}' class='btn btn-danger btn-sm'> Excluir </a></td>", p.IdPaciente);
                    dados.Append("</tr>");
                }

                return Json(dados.ToString());
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        public ActionResult Update(int id)
        {
            Paciente p = appPaciente.FindById(id);
            Session["telefoneSession"] = p.Telefone;
            return View(p);
        }

        public ActionResult Delete(int id)
        {
            Paciente p = appPaciente.FindById(id);
            
            if (p != null)
            {
                //foreach (Telefone t in p.Telefone)
                //{
                //    appTelefone.Delete(t);
                //}
                //foreach (Endereco e in p.Endereco)
                //{
                //    appEndereco.Delete(e);
                //}
                appPaciente.Delete(p);
            }
            return RedirectToAction("Index", "Paciente");
            //return View("Index");
        }

        public ActionResult ListarPacientes()
        {
            List<Paciente> p = appPaciente.FindAll();
            return Json(p, JsonRequestBehavior.AllowGet);
        }
    }
}