[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(SGH.Web.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(SGH.Web.App_Start.NinjectWebCommon), "Stop")]

namespace SGH.Web.App_Start
{
    using System;
    using System.Web;
    using System.Web.Http;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using Application.Contracts;
    using Application.Services;
    using Domain.Contracts.Services;
    using Domain.Services;
    using Domain.Contracts.Repository;
    using Infra.Repository.Repository;
    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                GlobalConfiguration.Configuration
                    .DependencyResolver = kernel.Get<System.Web.Http.Dependencies.IDependencyResolver>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            //inje��es de depend�ncia

            //N�vel Aplica��o
            kernel.Bind(typeof(IAppServiceBase<>)).To(typeof(AppServiceBase<>));
            kernel.Bind<IAppServiceFuncionario>().To<AppServiceFuncionario>();
            kernel.Bind<IAppServicePaciente>().To<AppServicePaciente>();
            kernel.Bind<IAppServiceEndereco>().To<AppServiceEndereco>();
            kernel.Bind<IAppServiceTelefone>().To<AppServiceTelefone>();
            kernel.Bind<IAppServiceMedico>().To<AppServiceMedico>();
            kernel.Bind<IAppServiceMedicacao>().To<AppServiceMedicacao>();
            kernel.Bind<IAppServiceConsulta>().To<AppServiceConsulta>();
            kernel.Bind<IAppServiceTratamento>().To<AppServiceTratamento>();

            //N�vel Dom�nio
            kernel.Bind(typeof(IDomainServiceBase<>)).To(typeof(DomainServiceBase<>));
            kernel.Bind<IDomainServiceFuncionario>().To<DomainServiceFuncionario>();
            kernel.Bind<IDomainServicePaciente>().To<DomainServicePaciente>();
            kernel.Bind<IDomainServiceEndereco>().To<DomainServiceEndereco>();
            kernel.Bind<IDomainServiceTelefone>().To<DomainServiceTelefone>();
            kernel.Bind<IDomainServiceMedico>().To<DomainServiceMedico>();
            kernel.Bind<IDomainServiceMedicacao>().To<DomainServiceMedicacao>();
            kernel.Bind<IDomainServiceConsulta>().To<DomainServiceConsulta>();
            kernel.Bind<IDomainServiceTratamento>().To<DomainServiceTratamento>();

            //N�vel InfraEstrutura
            kernel.Bind(typeof(IRepositoryBase<>)).To(typeof(RepositoryBase<>));
            kernel.Bind<IRepositoryFuncionario>().To<RepositoryFuncionario>();
            kernel.Bind<IRepositoryPaciente>().To<RepositoryPaciente>();
            kernel.Bind<IRepositoryEndereco>().To<RepositoryEndereco>();
            kernel.Bind<IRepositoryTelefone>().To<RepositoryTelefone>();
            kernel.Bind<IRepositoryMedico>().To<RepositoryMedico>();
            kernel.Bind<IRepositoryMedicacao>().To<RepositoryMedicacao>();
            kernel.Bind<IRepositoryConsulta>().To<RepositoryConsulta>();
            kernel.Bind<IRepositoryTratamento>().To<RepositoryTratamento>();

        }        
    }
}
