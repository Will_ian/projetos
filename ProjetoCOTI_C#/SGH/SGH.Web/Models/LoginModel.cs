﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SGH.Web.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Por favor, informe o login do usuário.")]
        [Display(Name = "Login:")]
        public string LoginAcesso { get; set; }

        [Required(ErrorMessage = "Por favor, informe a senha do usuário.")]
        [Display(Name = "Senha:")]
        public string SenhaAcesso { get; set; }
    }
}