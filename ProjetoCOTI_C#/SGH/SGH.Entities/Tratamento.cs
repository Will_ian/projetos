﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Entities
{
    public class Tratamento
    {
        public virtual int IdTratamento { get; set; }
        public virtual string Descricao { get; set; }
        public virtual int IdConsulta { get; set; }
        public virtual int IdMedicacao { get; set; }

        #region Relacionamentos
        public virtual Consulta Consulta { get; set; }
        public virtual Medicacao Medicacao { get; set; }
        #endregion
    }
}
