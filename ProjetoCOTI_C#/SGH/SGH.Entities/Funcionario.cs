﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Entities
{
    public class Funcionario
    {
        public virtual int IdFuncionario { get; set; }
        public virtual string Nome { get; set; }
        public virtual string Login { get; set; }
        public virtual string Senha { get; set; }
        public virtual DateTime DataCadastro { get; set; }

        [NotMapped]
        public string perfil
        {
            get { return Login.Split('_')[0]; }
        }
       
        [NotMapped]
        public string DataCadastroFormatada
        {
            get
            {
                return DataCadastro != null ? DataCadastro.ToString("dd/MM/yyyy") : string.Empty;
            }
        }
        #region Relacionamentos
        public virtual List<Paciente> Paciente { get; set; }
        public virtual List<Consulta> Consulta { get; set; }
        #endregion
    }
}
