﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Entities
{
    public class Telefone
    {
        public virtual int IdTelefone { get; set; }
        public virtual long NumeroTel { get; set; }
        public virtual string TipoTel { get; set; }
        public virtual int IdPaciente { get; set; }

        #region Relacionamentos
        public virtual Paciente Paciente { get; set; }
        #endregion
    }
}
