﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Entities
{
    public class Medico
    {
        public virtual int IdMedico { get; set; }
        public virtual string Nome { get; set; }
        public virtual int CRM { get; set; }
        public virtual string Descricao { get; set; }

        #region Relacionamentos
        public virtual List<Consulta> Consulta { get; set; }
        #endregion
    }
}
