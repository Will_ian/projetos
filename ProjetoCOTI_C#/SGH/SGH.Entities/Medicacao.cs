﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Entities
{
    public class Medicacao
    {
        public virtual int IdMedicacao { get; set; }
        public virtual string Nome { get; set; }
        public virtual string Descricao { get; set; }

        #region Relacionamentos
        public List<Tratamento> Tratamento { get; set; }
        #endregion
    }
}
