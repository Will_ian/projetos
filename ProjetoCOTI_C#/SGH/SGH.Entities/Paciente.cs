﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Entities
{
    public class Paciente
    {
        public virtual int IdPaciente { get; set; }
        public virtual string Nome { get; set; }
        public virtual string Email { get; set; }
        public virtual string Sexo { get; set; }
        public virtual DateTime DataNascimento { get; set; }
        public virtual int IdFuncionario { get; set; }

        #region Relacionamentos
        public virtual Funcionario Funcionario { get; set; }
        public virtual List<Endereco> Endereco { get; set; }
        public virtual List<Telefone> Telefone { get; set; }
        public virtual List<Consulta> Consulta { get; set; }
        #endregion
    }
}
