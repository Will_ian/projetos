﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Entities
{
    public class Consulta
    {
        public virtual int IdConsulta { get; set; }
        public virtual DateTime Data { get; set; }
        public virtual DateTime Hora { get; set; }
        public virtual decimal ValorPago { get; set; }
        public virtual int IdFuncionario { get; set; }
        public virtual int IdMedico { get; set; }
        public virtual int IdPaciente { get; set; }
        public virtual string Receita { get; set; }

        public int HoraFormatada
        {
            get
            {
                return Hora.Hour;
            }
        }

        #region Relacionamentos
        public virtual Funcionario Funcionario { get; set; }
        public virtual Paciente Paciente { get; set; }
        public virtual List<Tratamento> Tratamento { get; set; }
        public virtual Medico Medico { get; set; }
        #endregion
    }
}
