﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Util
{
    public class Criptografia
    {
        public static string EncriptarSenha(string senha)
        {
            MD5 md5 = new MD5CryptoServiceProvider();

            return BitConverter.ToString(
                    md5.ComputeHash(Encoding.UTF8.GetBytes(senha)))
                    .Replace("-", string.Empty);
        }
    }
}
