﻿using SGH.Domain.Contracts.Repository;
using SGH.Domain.Contracts.Services;
using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Domain.Services
{
    public class DomainServiceTratamento
        : DomainServiceBase<Tratamento>,
        IDomainServiceTratamento
    {
        private IRepositoryTratamento repositorio;

        public DomainServiceTratamento(IRepositoryTratamento repositorio)
            :base(repositorio)
        {
            this.repositorio = repositorio;
        }

        public Tratamento FindByIdConsulta(int id)
        {
            return this.repositorio.FindByIdConsulta(id);
        }

        public Tratamento FindByIdMedicacao(int id)
        {
            return this.repositorio.FindByIdMedicacao(id);
        }
    }
}
