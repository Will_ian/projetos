﻿using SGH.Domain.Contracts.Repository;
using SGH.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Domain.Services
{
    public class DomainServiceBase<TEntity> : IDomainServiceBase<TEntity>
        where TEntity : class
    {
        //atributo para a interface do repositório
        private IRepositoryBase<TEntity> repositorio;

        public DomainServiceBase(IRepositoryBase<TEntity> repositorio)
        {
            this.repositorio = repositorio;
        }

        public void Create(TEntity obj)
        {
            this.repositorio.Create(obj);
        }

        public void Update(TEntity obj)
        {
            this.repositorio.Update(obj);
        }

        public void Delete(TEntity obj)
        {
            this.repositorio.Delete(obj);
        }

        public List<TEntity> FindAll()
        {
            return this.repositorio.FindAll();
        }

        public TEntity FindById(int id)
        {
            return this.repositorio.FindById(id);
        }
    }
}
