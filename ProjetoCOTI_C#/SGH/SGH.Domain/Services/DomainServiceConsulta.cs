﻿using SGH.Domain.Contracts.Repository;
using SGH.Domain.Contracts.Services;
using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Domain.Services
{
    public class DomainServiceConsulta
        : DomainServiceBase<Consulta>,
        IDomainServiceConsulta
    {
        private IRepositoryConsulta repositorio;

        public DomainServiceConsulta(IRepositoryConsulta repositorio)
            :base(repositorio)
        {
            this.repositorio = repositorio;
        }
        public List<Consulta> FindAllByData(DateTime dataConsulta)
        {
            return this.repositorio.FindAllByData(dataConsulta);
        }

        public Consulta FindAllByDataHour(DateTime dataConsulta, DateTime horaConsulta)
        {
            return this.repositorio.FindAllByDataHour(dataConsulta, horaConsulta);
        }
        public List<Consulta> FindAllByDataAndMedico(DateTime dataConsulta, int idMedico)
        {
            return this.repositorio.FindAllByDataAndMedico(dataConsulta, idMedico);
        }

        public List<Consulta> FindAllBetweenData(DateTime dataInicial, DateTime dataFinal)
        {
            return this.repositorio.FindAllBetweenData(dataInicial, dataFinal);
        }
    }
}
