﻿using SGH.Domain.Contracts.Repository;
using SGH.Domain.Contracts.Services;
using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Domain.Services
{
    public class DomainServiceFuncionario
        : DomainServiceBase<Funcionario>,
          IDomainServiceFuncionario
    {
        //atributo para a interface do repositório
        private IRepositoryFuncionario repositorio;

        public DomainServiceFuncionario(IRepositoryFuncionario repositorio)
            :base(repositorio)
        {
            this.repositorio = repositorio;
        }

        public Funcionario FindByEmail(string login)
        {
            return this.repositorio.FindByEmail(login);
        }

        public Funcionario FindByEmailPassword(string login, string senha)
        {
            return this.repositorio.FindByEmailPassword(login, senha);
        }

        public List<Funcionario> FindAllByName(string nome)
        {
            return this.repositorio.FindAllByName(nome);
        }
    }
}
