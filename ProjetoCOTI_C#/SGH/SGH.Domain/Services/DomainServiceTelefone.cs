﻿using SGH.Domain.Contracts.Repository;
using SGH.Domain.Contracts.Services;
using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Domain.Services
{
    public class DomainServiceTelefone 
        : DomainServiceBase<Telefone>,
          IDomainServiceTelefone
    {
        //atributo para a interface do repositório
        private IRepositoryTelefone repositorio;

        public DomainServiceTelefone(IRepositoryTelefone repositorio)
            :base(repositorio)
        {
            this.repositorio = repositorio;
        }

        public List<Telefone> FindByIdPaciente(int id)
        {
            return this.repositorio.FindByIdPaciente(id);
        }
    }
}
