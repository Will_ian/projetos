﻿using SGH.Domain.Contracts.Repository;
using SGH.Domain.Contracts.Services;
using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Domain.Services
{
    public class DomainServicePaciente : DomainServiceBase<Paciente>,
          IDomainServicePaciente
    {
        //atributo para a interface do repositório
        private IRepositoryPaciente repositorio;

        public DomainServicePaciente(IRepositoryPaciente repositorio)
            :base(repositorio)
        {
            this.repositorio = repositorio;
        }

        public List<Paciente> FindAllByNome(string nome)
        {
            return this.repositorio.FindAllByNome(nome);
        }
    }
}
