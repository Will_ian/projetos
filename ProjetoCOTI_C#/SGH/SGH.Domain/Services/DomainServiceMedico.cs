﻿using SGH.Domain.Contracts.Repository;
using SGH.Domain.Contracts.Services;
using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Domain.Services
{
    public class DomainServiceMedico
        : DomainServiceBase<Medico>,
          IDomainServiceMedico
    {
        //atributo para a interface do repositório
        private IRepositoryMedico repositorio;

        public DomainServiceMedico(IRepositoryMedico repositorio)
            : base(repositorio)
        {
            this.repositorio = repositorio;
        }

        public List<Medico> FindAllByName(string nome)
        {
            return this.repositorio.FindAllByName(nome);
        }

        public Medico FindByCrm(int CRM)
        {
            return this.repositorio.FindByCrm(CRM);
        }
    }
}
