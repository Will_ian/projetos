﻿using SGH.Domain.Contracts.Repository;
using SGH.Domain.Contracts.Services;
using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Domain.Services
{
    public class DomainServiceMedicacao
        : DomainServiceBase<Medicacao>,
        IDomainServiceMedicacao
    {
        //atributo para a interface do repositório
        private IRepositoryMedicacao repositorio;

        public DomainServiceMedicacao(IRepositoryMedicacao repositorio)
            :base(repositorio)
        {
            this.repositorio = repositorio;
        }

        public List<Medicacao> FindAllByName(string nome)
        {
            return this.repositorio.FindAllByName(nome);
        }
    }
}
