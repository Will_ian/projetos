﻿using SGH.Domain.Contracts.Repository;
using SGH.Domain.Contracts.Services;
using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Domain.Services
{
    public class DomainServiceEndereco
        : DomainServiceBase<Endereco>,
          IDomainServiceEndereco
    {
        //atributo para a interface do repositório
        private IRepositoryEndereco repositorio;

        public DomainServiceEndereco(IRepositoryEndereco repositorio)
            :base(repositorio)
        {
            this.repositorio = repositorio;
        }

        public List<Endereco> FindByIdPaciente(int id)
        {
            return this.repositorio.FindByIdPaciente(id);
        }
    }
}
