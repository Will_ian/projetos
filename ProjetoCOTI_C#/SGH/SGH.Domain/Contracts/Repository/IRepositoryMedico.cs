﻿using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Domain.Contracts.Repository
{
    public interface IRepositoryMedico
        : IRepositoryBase<Medico>
    {
        List<Medico> FindAllByName(string nome);

        Medico FindByCrm(int CRM);
    }
}
