﻿using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Domain.Contracts.Repository
{
    public interface IRepositoryEndereco
         : IRepositoryBase<Endereco>
    {
        List<Endereco> FindByIdPaciente(int id);
    }
}
