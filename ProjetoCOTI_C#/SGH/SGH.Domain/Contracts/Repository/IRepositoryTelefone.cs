﻿using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Domain.Contracts.Repository
{
    public interface IRepositoryTelefone
         : IRepositoryBase<Telefone>
    {
        List<Telefone> FindByIdPaciente(int id);
    }
}
