﻿using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Domain.Contracts.Services
{
    public interface IDomainServiceFuncionario
        : IDomainServiceBase<Funcionario>
    {
        Funcionario FindByEmailPassword(string login, string senha);

        Funcionario FindByEmail(string login);

        List<Funcionario> FindAllByName(string nome);
    }
}
