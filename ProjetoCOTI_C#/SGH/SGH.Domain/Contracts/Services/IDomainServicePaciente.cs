﻿using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Domain.Contracts.Services
{
    public interface IDomainServicePaciente
        : IDomainServiceBase<Paciente>
    {
        List<Paciente> FindAllByNome(string nome);
    }
}
