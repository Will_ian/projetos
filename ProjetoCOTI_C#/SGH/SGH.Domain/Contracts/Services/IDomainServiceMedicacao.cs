﻿using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Domain.Contracts.Services
{
    public interface IDomainServiceMedicacao
        : IDomainServiceBase<Medicacao>
    {
        List<Medicacao> FindAllByName(string nome);
    }
}
