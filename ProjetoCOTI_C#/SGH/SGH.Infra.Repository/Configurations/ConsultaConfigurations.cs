﻿using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Infra.Repository.Configurations
{
    public class ConsultaConfigurations : EntityTypeConfiguration<Consulta>
    {
        public ConsultaConfigurations()
        {
            ToTable("CONSULTA");

            HasKey(c => c.IdConsulta);

            Property(c => c.Data)
                .IsRequired();

            Property(c => c.Hora)
                .IsRequired();

            Property(c => c.ValorPago)
                .HasPrecision(18,2)
                .IsRequired();

            Property(c => c.Receita)
                .HasMaxLength(255)
                .IsRequired();

            #region Relacionamentos

            HasRequired(c => c.Funcionario)
                .WithMany(f => f.Consulta)
                .HasForeignKey(c => c.IdFuncionario);

            HasRequired(c => c.Medico)
                .WithMany(m => m.Consulta)
                .HasForeignKey(c => c.IdMedico);

            HasRequired(c => c.Paciente)
                .WithMany(p => p.Consulta)
                .HasForeignKey(c => c.IdPaciente);

            #endregion
        }
    }
}
