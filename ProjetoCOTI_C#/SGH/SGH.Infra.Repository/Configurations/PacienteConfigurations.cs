﻿using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Infra.Repository.Configurations
{
    public class PacienteConfigurations : EntityTypeConfiguration<Paciente>
    {
        public PacienteConfigurations()
        {
            ToTable("PACIENTE");

            HasKey(p => p.IdPaciente);

            Property(p => p.Nome)
                .HasMaxLength(50)
                .IsRequired();

            Property(p => p.Email)
                .HasMaxLength(50)
                .IsRequired();

            Property(p => p.Sexo)
                .HasMaxLength(2)
                .IsRequired();

            Property(p => p.DataNascimento)
                .IsRequired();

            #region Relacionamentos

            HasRequired(p => p.Funcionario)
                .WithMany(f => f.Paciente)
                .HasForeignKey(p => p.IdFuncionario);

            #endregion
        }
    }
}
