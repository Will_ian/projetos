﻿using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Infra.Repository.Configurations
{
    public class MedicoConfigurations : EntityTypeConfiguration<Medico>
    {
        public MedicoConfigurations()
        {

            ToTable("MEDICO");

            HasKey(m => m.IdMedico);

            Property(m => m.Nome)
                .HasMaxLength(50)
                .IsRequired();

            Property(m => m.CRM)
                .IsRequired();

            Property(m => m.Descricao)
                .HasMaxLength(100)
                .IsRequired();
        }
    }
}
