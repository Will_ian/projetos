﻿using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Infra.Repository.Configurations
{
    public class EnderecoConfigurations : EntityTypeConfiguration<Endereco>
    {
        public EnderecoConfigurations()
        {
            ToTable("ENDERECO");

            HasKey(e => e.IdEndereco);

            Property(e => e.Logradouro)
                .HasMaxLength(100)
                .IsRequired();

            Property(e => e.Bairro)
                .HasMaxLength(50)
                .IsRequired();

            Property(e => e.Cidade)
                .HasMaxLength(50)
                .IsRequired();

            Property(e => e.Estado)
                .HasMaxLength(50)
                .IsRequired();

            Property(e => e.Cep)
                .IsRequired();

            #region Relacionamentos

            HasRequired(e => e.Paciente)
                .WithMany(p => p.Endereco)
                .HasForeignKey(e => e.IdPaciente);

            #endregion
        }
    }
}
