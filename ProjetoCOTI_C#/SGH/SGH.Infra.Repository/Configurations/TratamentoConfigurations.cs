﻿using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Infra.Repository.Configurations
{
    public class TratamentoConfigurations : EntityTypeConfiguration<Tratamento>
    {
        public TratamentoConfigurations()
        {

            ToTable("TRATAMENTO");

            HasKey(t => t.IdTratamento);

            Property(t => t.Descricao)
                .HasMaxLength(255)
                .IsRequired();

            #region Relacionamentos

            HasRequired(t => t.Consulta)
                .WithMany(c => c.Tratamento)
                .HasForeignKey(t => t.IdConsulta);

            HasRequired(t => t.Medicacao)
                .WithMany(m => m.Tratamento)
                .HasForeignKey(t => t.IdMedicacao);

            #endregion
        }
    }
}
