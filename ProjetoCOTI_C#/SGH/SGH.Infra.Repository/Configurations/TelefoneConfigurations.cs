﻿using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Infra.Repository.Configurations
{
    public class TelefoneConfigurations : EntityTypeConfiguration<Telefone>
    {
        public TelefoneConfigurations()
        {
            ToTable("TELEFONE");

            HasKey(t => t.IdTelefone);

            Property(t => t.NumeroTel)
                .IsRequired();

            Property(t => t.TipoTel)
                .HasMaxLength(15)
                .IsRequired();

            #region Relacionamentos

            HasRequired(t => t.Paciente)
                .WithMany(p => p.Telefone)
                .HasForeignKey(t => t.IdPaciente);

            #endregion
        }
    }
}
