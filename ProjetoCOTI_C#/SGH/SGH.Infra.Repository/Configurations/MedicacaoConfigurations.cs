﻿using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Infra.Repository.Configurations
{
    public class MedicacaoConfigurations : EntityTypeConfiguration<Medicacao>
    {
        public MedicacaoConfigurations()
        {

            ToTable("MEDICACAO");

            HasKey(m => m.IdMedicacao);

            Property(m => m.Nome)
                .HasMaxLength(50)
                .IsRequired();

            Property(m => m.Descricao)
                .HasMaxLength(255)
                .IsRequired();
        }
    }
}
