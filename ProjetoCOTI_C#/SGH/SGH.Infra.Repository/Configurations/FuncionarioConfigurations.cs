﻿using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Infra.Repository.Configurations
{
    public class FuncionarioConfigurations : EntityTypeConfiguration<Funcionario>
    {
        public FuncionarioConfigurations()
        {
            ToTable("FUNCIONARIO");

            HasKey(f => f.IdFuncionario);

            Property(f => f.Nome)
                .HasMaxLength(50)
                .IsRequired();

            Property(f => f.Login)
                .HasMaxLength(50)
                .IsRequired();

            Property(f => f.Senha)
                .IsRequired();

            Property(f => f.DataCadastro)
                .IsRequired();
        }
    }
}
