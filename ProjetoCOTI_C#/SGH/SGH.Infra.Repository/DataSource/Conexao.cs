﻿using SGH.Entities;
using SGH.Infra.Repository.Configurations;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Infra.Repository.DataSource
{
    public class Conexao : DbContext
    {
        public Conexao()
            : base(ConfigurationManager.ConnectionStrings["sgh"].ConnectionString)
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //alterar definições do entityframework
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            //classes de mapeamento
            modelBuilder.Configurations.Add(new FuncionarioConfigurations());
            modelBuilder.Configurations.Add(new PacienteConfigurations());
            modelBuilder.Configurations.Add(new EnderecoConfigurations());
            modelBuilder.Configurations.Add(new TelefoneConfigurations());
            modelBuilder.Configurations.Add(new ConsultaConfigurations());
            modelBuilder.Configurations.Add(new MedicoConfigurations());
            modelBuilder.Configurations.Add(new TratamentoConfigurations());
            modelBuilder.Configurations.Add(new MedicacaoConfigurations());          
        }

        public DbSet<Funcionario> Funcionario { get; set; }
        public DbSet<Paciente> Paciente { get; set; }
        public DbSet<Endereco> Endereco { get; set; }
        public DbSet<Telefone> Telefone { get; set; }
        public DbSet<Consulta> Consulta { get; set; }
        public DbSet<Medico> Medico { get; set; }
        public DbSet<Tratamento> Tratamento { get; set; }
        public DbSet<Medicacao> Medicacao { get; set; }       
    }
}
