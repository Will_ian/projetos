﻿using SGH.Domain.Contracts.Repository;
using SGH.Entities;
using SGH.Infra.Repository.DataSource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Infra.Repository.Repository
{
    public class RepositoryFuncionario
        : RepositoryBase<Funcionario>,
        IRepositoryFuncionario
    {
        public Funcionario FindByEmail(string login)
        {
            return Con.Funcionario
                 .Where(f => f.Login.Equals(login))
                 .FirstOrDefault();
        }

        public Funcionario FindByEmailPassword(string login, string senha)
        {
            return Con.Funcionario
                 .Where(f => f.Login.Equals(login) 
                        && f.Senha.Equals(senha))
                        .FirstOrDefault();
        }

        //método para retornar paciente pelo nome..
        public List<Funcionario> FindAllByName(string nome)
        {
            using (Conexao Con = new Conexao())
            {
                return Con.Funcionario
                .Where(f => f.Nome.Contains(nome))
               .ToList();
            }
        } 
    }
}
