﻿using SGH.Domain.Contracts.Repository;
using SGH.Entities;
using SGH.Infra.Repository.DataSource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Infra.Repository.Repository
{
    public class RepositoryPaciente : RepositoryBase<Paciente>,
        IRepositoryPaciente
    {
        //método para retornar paciente pelo nome..
        public List<Paciente> FindAllByNome(string nome)
        {
            using (Conexao Con = new Conexao())
            {
                return Con.Paciente
                .Where(p => p.Nome.Contains(nome))
               .ToList();
            }
        }

        public List<Paciente> FindAllBetweenData(DateTime dataInicial, DateTime dataFinal)
        {
            using (Conexao Con = new Conexao())
            {
                return Con.Paciente
                .Where(p => p.DataNascimento >= dataInicial && p.DataNascimento <= dataFinal)
               .ToList();
            }
        }
    }
}
