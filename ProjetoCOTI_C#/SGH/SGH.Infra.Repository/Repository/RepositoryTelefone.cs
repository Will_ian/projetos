﻿using SGH.Domain.Contracts.Repository;
using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Infra.Repository.Repository
{
    public class RepositoryTelefone
        : RepositoryBase<Telefone>,
        IRepositoryTelefone
    {
        public List<Telefone> FindByIdPaciente(int id)
        {
            return Con.Telefone
                .Where(t => t.IdPaciente == id)
                .ToList();
        }
    }
}
