﻿using SGH.Domain.Contracts.Repository;
using SGH.Entities;
using SGH.Infra.Repository.DataSource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Infra.Repository.Repository
{
    public class RepositoryMedicacao
        : RepositoryBase<Medicacao>,
        IRepositoryMedicacao
    {
        //método para retornar paciente pelo nome..
        public List<Medicacao> FindAllByName(string nome)
        {
            using (Conexao Con = new Conexao())
            {
                return Con.Medicacao
                .Where(m => m.Nome.Contains(nome))
               .ToList();
            }
        }
    }
}
