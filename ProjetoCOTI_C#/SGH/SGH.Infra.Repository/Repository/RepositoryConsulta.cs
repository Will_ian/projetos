﻿using SGH.Domain.Contracts.Repository;
using SGH.Entities;
using SGH.Infra.Repository.DataSource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Infra.Repository.Repository
{
    public class RepositoryConsulta
        : RepositoryBase<Consulta>,
        IRepositoryConsulta
    {
        public List<Consulta> FindAllBetweenData(DateTime dataInicial, DateTime dataFinal)
        {
            using (Conexao Con = new Conexao())
            {
                return Con.Consulta
                .Where(c => c.Data >= dataInicial && c.Data <= dataFinal)
               .ToList();
            }
        }

        public List<Consulta> FindAllBetweenDataHour(DateTime dataInicial, DateTime dataFinal)
        {
            using (Conexao Con = new Conexao())
            {
                return Con.Consulta
                .Where(c => c.Hora >= dataInicial && c.Hora <= dataFinal)
               .ToList();
            }
        }

        public List<Consulta> FindAllByData(DateTime dataConsulta)
        {
            using (Conexao Con = new Conexao())
            {
                return Con.Consulta
                .Where(c => c.Data == dataConsulta)
               .ToList();
            }
        }

        public List<Consulta> FindAllByDataAndMedico(DateTime dataConsulta, int idMedico)
        {
            using (Conexao Con = new Conexao())
            {
                return Con.Consulta
                .Where(c => c.Data == dataConsulta 
                            && c.IdMedico == idMedico)
               .ToList();
            }
        }

        public Consulta FindAllByDataHour(DateTime dataConsulta, DateTime horaConsulta)
        {
            using (Conexao Con = new Conexao())
            {
                return Con.Consulta
                .Where(c => c.Data == dataConsulta && c.Hora == horaConsulta)
               .FirstOrDefault();
            }
        }
    }
}
