﻿using SGH.Domain.Contracts.Repository;
using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Infra.Repository.Repository
{
    public class RepositoryTratamento
        : RepositoryBase<Tratamento>,
        IRepositoryTratamento
    {
        public Tratamento FindByIdConsulta(int id)
        {
            return Con.Tratamento
                .Where(t => t.IdConsulta == id)
                .FirstOrDefault();
        }

        public Tratamento FindByIdMedicacao(int id)
        {
            throw new NotImplementedException();
        }
    }
}
