﻿using SGH.Domain.Contracts.Repository;
using SGH.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Infra.Repository.Repository
{
    public class RepositoryEndereco
        : RepositoryBase<Endereco>,
        IRepositoryEndereco
    {
        public List<Endereco> FindByIdPaciente(int id)
        {
            return Con.Endereco
                 .Where(e => e.IdPaciente == id)
                 .ToList();
        }
    }
}
