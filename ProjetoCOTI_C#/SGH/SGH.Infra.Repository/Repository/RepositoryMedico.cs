﻿using SGH.Domain.Contracts.Repository;
using SGH.Entities;
using SGH.Infra.Repository.DataSource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Infra.Repository.Repository
{
    public class RepositoryMedico
        : RepositoryBase<Medico>,
        IRepositoryMedico
    {

        //método para retornar paciente pelo nome..
        public List<Medico> FindAllByName(string nome)
        {
            using (Conexao Con = new Conexao())
            {
                return Con.Medico
                .Where(m => m.Nome.Contains(nome))
               .ToList();
            }
        }

        public Medico FindByCrm(int CRM)
        {
            using (Conexao Con = new Conexao())
            {
                return Con.Medico
                .Where(m => m.CRM == CRM)
               .FirstOrDefault();
            }
        }
    }
}
