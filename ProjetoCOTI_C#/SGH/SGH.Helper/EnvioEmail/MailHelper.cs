﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace SGH.Helper.EnvioEmail
{
    /// <summary>
    /// Classe com métodos responsáveis por parâmetros e envio efetivo de e-mails para usuários
    /// </summary>
    public class MailHelper
    {
        private static AlternateView altView;

        /// <summary>
        /// Cria o conteúdo do e-mail, renderizando-o
        /// </summary>
        /// <param name="subject">Assunto do Informe</param>
        /// <param name="multimidia">Conteúdo Rich Text do Informe</param>
        private static void CreateEmails(string multimidia)
        {
            var mailToGroups = HtmlMailBody(HtmlContent(multimidia));
            EmailLayoutRender(mailToGroups);
        }

        /// <summary>
        /// Anexa o cabeçalho, o conteúdo variável do e-mail e o rodapé formando assim o corpo do e-mail
        /// </summary>
        /// <param name="customMailContent">E-mail</param>
        /// <returns>Html do corpo do e-mail</returns>
        private static string HtmlMailBody(string customMailContent)
        {
            try
            {
                System.Text.StringBuilder html = new System.Text.StringBuilder();

                html.Append("<html><body>");
                html.Append("<table width='600' height='450' border='0' cellpadding='0' cellspacing='0' align='center'>");
                html.Append(HtmlMailHeader());

                html.Append("<tr><td valign='top'>");
                html.Append("<table border='0' cellpadding='0' cellspacing='0' align='center' width='95%' style='font-family:Arial,sans-serif;font-weight:normal;border-collapse:collapse;margin-top:0px'>");
                html.Append("<tr><td style='border-bottom:4px solid #0076bd'>");
                html.Append("<h2 style='color:#666565;font-size:28px;display:block;font-weight:normal;padding:0px;margin:0px 0px 5px 0px'>RECUPERAÇÃO DE SENHA</h2>");
                html.Append("</td></tr></table></td></tr>");
                html.Append("<tr><td valign='top'>");
                html.Append("<table border='0' cellpadding='15' cellspacing='0' align='center' width='95%' style='font-family:Arial,sans-serif;font-weight:normal;border-collapse:collapse;'>");
                html.Append("<tr><td>");
                html.Append(customMailContent);
                html.Append("</td></tr></table></td></tr>");
                html.Append("<tr><td height='30'></td></tr>");
                html.Append(HtmlMailFooter());
                html.Append("</table></body></html>");

                return html.ToString();
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Cabeçalho do e-mail
        /// </summary>
        /// <returns>Html do cabeçalh</returns>
        private static string HtmlMailHeader()
        {
            try
            {
                System.Text.StringBuilder html = new System.Text.StringBuilder();

                html.Append("<tr height='80'>");
                html.Append("<td valign='top'>");
                html.Append("<img src=\"" + Configurations.HeaderImage + "\" alt='globosat newsletter' align='top' width='600' height='60' />");
                html.Append("</td></tr>");

                return html.ToString();
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Rodapé do e-mail
        /// </summary>
        /// <returns>Html do rodapé</returns>
        private static string HtmlMailFooter()
        {
            try
            {
                System.Text.StringBuilder html = new System.Text.StringBuilder();

                html.Append("<tr><td height='66' bgcolor='#e0e0e0'>");
                html.Append("<table border='0' cellpadding='0' cellspacing='0' width='100%' align='center' style='font: bold 8px Verdana;color:#808080;margin-left:20px'>");              
                html.Append("<tr><td>SGH Ltda. Todos os direitos reservados. 2003 - " + DateTime.Now.Year + "</td></tr>");
                html.Append("<tr><td>Politica de privacidade - Termos de Uso</td></tr></table></td></tr>");

                return html.ToString();
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Conteúdo do e-mail: assunto, texto e imagem
        /// </summary>
        /// <param name="subject">Assunto do Informe</param>
        /// <param name="multimidia">Corpo do Informe</param>
        /// <returns></returns>
        private static string HtmlContent(string password)
        {
            try
            {
                System.Text.StringBuilder html = new System.Text.StringBuilder();
                html.Append("<h2 style='color:#666565;font-size:14px;display:block;font-weight:normal;padding:0px;margin:0px 0px 5px 0px'>Nova senha de acesso: " + password + "</h2>");

                return html.ToString();
            }
            catch (Exception)
            {       
                return string.Empty;
            }
        }

        /// <summary>
        /// Cria alternateView que renderiza o conteúdo do e-mail em Html
        /// </summary>
        /// <param name="mailBody">Corpo do e-mail</param>
        /// <returns></returns>
        private static void EmailLayoutRender(string mailBody)
        {
            try
            {
                altView = AlternateView.CreateAlternateViewFromString(mailBody, null, System.Net.Mime.MediaTypeNames.Text.Html);
            }
            catch (Exception)
            {
                
            }
        }

        /// <summary>
        /// Envia o e-mail do respectivo item da lista
        /// </summary>
        /// <param name="item">Item da lista</param>
        /// <param name="emails">E-mails dos usuários</param>
        /// <returns>True para envio com sucesso, false para falha no envio</returns>
        public static bool SendMail(string mailBody, string email)
        {
            try
            {
                if (mailBody != null)
                {
                    CreateEmails(mailBody);
                    using (MailMessage mail = CreateMail(email))
                    {                      
                        using (var client = new SmtpClient())
                        {
                            client.DeliveryMethod = SmtpDeliveryMethod.Network;
                            client.UseDefaultCredentials = false;

                            client.Credentials = new NetworkCredential(Configurations.EmailFrom, Configurations.EmailPassword);

                            client.Host = Configurations.EmailHost;
                            client.Port = Convert.ToInt32(Configurations.EmailPort);
                            client.EnableSsl = true;

                            client.Send(mail);
                            return true;
                        }
                    }
                }
                return false;

            }
            catch (Exception ex)
            {                
                return false;
            }
        }

        /// <summary>
        /// Cria os objetos MailMessage utilizados para envio de e-mail
        /// </summary>
        /// <param name="item">Item da lista</param>
        /// <param name="emails">E-mails dos usuários</param>
        /// <param name="mailBody">Corpo do e-mail</param>
        /// <returns>MailMessage</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "Em caso de Dispose, teremos perda do objeto mail")]
        private static MailMessage CreateMail(string email)
        {
            try
            {
                MailMessage mail = new MailMessage();

                mail.From = new MailAddress("no-replay@sgh.com.br");
                
                mail.To.Add(email);
               
                mail.IsBodyHtml = true;
                mail.AlternateViews.Add(altView);

                mail.Subject = Configurations.TitleItem;

                return mail;
            }
            catch (Exception)
            {
               
                throw;
            }
        }
    }
}
